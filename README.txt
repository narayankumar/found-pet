
build2014092101
- image field made compulsory
- views changes
  - empty global area text deleted
  - comment field in teasers view deleted
  - field pet_reunited hide when empty (for css to work fine)
  - deleted body field from teasers view
  - included 'click for details' in teasers view (link to content field)
  - search by city exposed filter added to teasers view

build2014091801
- made title 'pets found' in teasers view
- gave it class=black, h4, second word strong
- teasers view - re-united message as red & strong
- added name and posted date to match lost pet view
- changed link wording to 'report found pet'

build2014091602
- found pet - provide comments box
- found pet - hide share this label

7.x-1.0-dev1
- initial commit on sep 12 2014

<?php
/**
 * @file
 * found_pet.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function found_pet_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function found_pet_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function found_pet_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: ad_block_found_pets
  $nodequeues['ad_block_found_pets'] = array(
    'name' => 'ad_block_found_pets',
    'title' => 'Ad block - Found pets',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: ad_blocks_lost_found_page
  $nodequeues['ad_blocks_lost_found_page'] = array(
    'name' => 'ad_blocks_lost_found_page',
    'title' => 'Ad blocks - lost-found page',
    'subqueue_title' => '',
    'size' => 4,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'advertisement',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function found_pet_flag_default_flags() {
  $flags = array();
  // Exported flag: "Pet Re-united".
  $flags['pet_re_united'] = array(
    'entity_type' => 'node',
    'title' => 'Pet Re-united',
    'global' => 1,
    'types' => array(
      0 => 'found_pet',
    ),
    'flag_short' => 'Set status as \'RE-UNITED\' with Parent',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel \'RE-UNITED\' status',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => 'Re-united',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'diff_standard' => 0,
      'node_gallery_node_thumbnail' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => 'own',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'flag_confirmation' => 'Are you sure you want to set status as \'RE-UNITED\'?',
    'unflag_confirmation' => 'Are you sure you want to cancel the \'RE-UNITED\' status?',
    'module' => 'found_pet',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function found_pet_node_info() {
  $items = array(
    'found_pet' => array(
      'name' => t('Found pet'),
      'base' => 'node_content',
      'description' => t('Use this to report an animal or bird you have found which you feel has a legitimate owner who should be intimated'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * found_pet.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function found_pet_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'found_pets_blocks';
  $context->description = 'Ad blocks on found pets nodes and teasers page';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'found_pet' => 'found_pet',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_4' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_4',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Ad blocks on found pets nodes and teasers page');
  $export['found_pets_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'lost_found_page_blocks';
  $context->description = 'blocks on combined lost-found panels page';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'lost-found' => 'lost-found',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-ad_block_nodequeues-block_10' => array(
          'module' => 'views',
          'delta' => 'ad_block_nodequeues-block_10',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('blocks on combined lost-found panels page');
  $export['lost_found_page_blocks'] = $context;

  return $export;
}

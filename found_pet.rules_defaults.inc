<?php
/**
 * @file
 * found_pet.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function found_pet_default_rules_configuration() {
  $items = array();
  $items['rules_lost_found_creation_mail_to_editor'] = entity_import('rules_config', '{ "rules_lost_found_creation_mail_to_editor" : {
      "LABEL" : "Lost\\/Found\\/Adopt\\/Directory\\/Contest\\/Opinion creation mail to Editor",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_insert--found_pet" : { "bundle" : "found_pet" },
        "node_insert--lost_pet" : { "bundle" : "lost_pet" },
        "node_insert--pet_for_adoption" : { "bundle" : "pet_for_adoption" },
        "node_insert--directory_entry" : { "bundle" : "directory_entry" },
        "node_insert--photo_contest_entry" : { "bundle" : "photo_contest_entry" },
        "node_insert--video_contest_entry" : { "bundle" : "video_contest_entry" },
        "node_insert--opinion" : { "bundle" : "opinion" }
      },
      "IF" : [
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "3" : "3", "4" : "4", "5" : "5" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "Your submission has been sent to the Editor for approval. You will be notified when it is published." } },
        { "mail" : {
            "to" : "editor@gingertail.in",
            "subject" : "New \\u0027[node:content-type]\\u0027 submission",
            "message" : "hi:\\r\\n\\r\\n[node:author] has submitted \\u0027[node:title]\\u0027 to be considered for publication:\\r\\n\\r\\nYou can visit it at: [node:url]\\r\\n\\r\\nYou can edit it and change its status to \\u0027Published\\u0027 if you decide to.\\r\\n\\r\\n-- Your friendly robot at Gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_lost_found_publication_mail_to_user'] = entity_import('rules_config', '{ "rules_lost_found_publication_mail_to_user" : {
      "LABEL" : "Lost\\/Found\\/Adoption publication mail to User",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : {
        "node_update--lost_pet" : { "bundle" : "lost_pet" },
        "node_update--found_pet" : { "bundle" : "found_pet" },
        "node_update--pet_for_adoption" : { "bundle" : "pet_for_adoption" }
      },
      "IF" : [
        { "data_is" : { "data" : [ "node-unchanged:status" ], "value" : "0" } },
        { "data_is" : { "data" : [ "node:status" ], "value" : "1" } },
        { "NOT user_has_role" : {
            "account" : [ "node:author" ],
            "roles" : { "value" : { "3" : "3", "4" : "4", "5" : "5" } },
            "operation" : "OR"
          }
        }
      ],
      "DO" : [
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "A mail has been sent to [node:author] to notify publication" } },
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "Your submission has been published",
            "message" : "hi [node:author]:\\r\\n\\r\\nYour submission \\u0027[node:title]\\u0027 has been accepted for publication.\\r\\n\\r\\nYou can visit it at: [node:url]\\r\\n\\r\\nCheers. Keep it coming!\\r\\n\\r\\n-- Editor at Gingertail.in",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_reunited_flag_reveals_reunited_message'] = entity_import('rules_config', '{ "rules_reunited_flag_reveals_reunited_message" : {
      "LABEL" : "Re-united flag reveals \\u0027re-united\\u0027 message",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "flag" ],
      "ON" : { "flag_flagged_pet_re_united" : [] },
      "IF" : [
        { "entity_has_field" : { "entity" : [ "flagged-node" ], "field" : "field_found_pet_reunited" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "flagged-node:field-found-pet-reunited" ],
            "value" : "RE-UNITED"
          }
        }
      ]
    }
  }');
  return $items;
}
